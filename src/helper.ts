import { DisplayObject } from "pixi.js"

export type callback = (...args: any[]) => any

export function toggleFullScreen() {
    if (!document.fullscreenElement) {
        document.documentElement.requestFullscreen()
    } else if (document.exitFullscreen) {
        document.exitFullscreen()
    }
}

export const FAKE_LEADERBOARD = [
    {name: 'Test 1', score: '123456'},
    {name: 'Test 2', score: '103456'},
    {name: 'Test 3', score: '82345'},
    {name: 'Test 4', score: '72345'},
    {name: 'Test 5', score: '62345'},
    {name: 'Test 6', score: '52345'},
    {name: 'Test 7', score: '42345'},
    {name: 'Test 8', score: '32345'},
    {name: 'Test 9', score: '22345'},
    {name: 'Test 10', score: '12345'},
]

export const STARS_CONFIG = [
    {pos: [-400, -300], scale: 1},
    {pos: [-500, -100], scale: 0.8},
    {pos: [-500, 100], scale: 1.2},
    {pos: [-400, 300], scale: 1},
    {pos: [400, -300], scale: 1},
    {pos: [500, -100], scale: 1.2},
    {pos: [500, 100], scale: 0.8},
    {pos: [400, 300], scale: 1},
]

export const COIN_VERTICES = [
    [1, 1, 1, 1, 1],
    [8, 8, 8, 8, 8],
    [1, 3, 4, 3, 1],
    [1, 3, 6, 9, 12],
    [12, 9, 6, 3, 1],
]

export const collision = (object1: DisplayObject, object2: DisplayObject) => {
    const bounds1 = object1.getBounds();
    const bounds2 = object2.getBounds();

    return bounds1.x < bounds2.x + bounds2.width
        && bounds1.x + bounds1.width > bounds2.x
        && bounds1.y < bounds2.y + bounds2.height
        && bounds1.y + bounds1.height > bounds2.y;
}

export const METER = 250

export const SPEED = METER / 15

export interface IActivated {
    activated: boolean
}