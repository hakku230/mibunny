import WebFontLoader from 'webfontloader'
import { Application, Assets } from "pixi.js"
import { Game } from './components/Game'
import { SoundManager } from './components/SoundManager'

const loadResources = async () => {
    const spritesheets = ['environment', 'ui']

    Assets.addBundle('static', {
        'back_rocks': './assets/back_rocks.png',
        'bg_gradient': './assets/bg_gradient.png',
        'bg_preloader': './assets/bg_preloader.png',
        'bg_sun': './assets/bg_sun.png',
        'bunny': './assets/bunny.png',
        'info_plate_big': './assets/info_plate_big.png',
        'mi_bunny_ride_logo': './assets/mi_bunny_ride_logo.png',
        'rays': './assets/rays.png',
        'jumpboard': './assets/jumpboard.png'
    })

    await Assets.loadBundle('static')

    for (let i = 0; i < spritesheets.length; i++) {
        const resName = spritesheets[i]

        await Assets.load('./assets/' + resName + '.json')
    }
}

WebFontLoader.load({
    custom: {
          families: ['ZubiloBlack']
    },
    loading: () => {},
    active: async () => {
        await loadResources()

        const app = new Application<HTMLCanvasElement>({ resizeTo: window, background: 0x8bc1fd })

        globalThis.__PIXI_APP__ = app

        document.querySelector('body').appendChild(app.view)

        SoundManager.init()

        new Game(app)
    }
})