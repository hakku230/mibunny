import { Container, Graphics, Resource, Sprite, Texture } from "pixi.js";

export class Obstacle extends Container {
    activated: boolean = false

    private _body: Graphics
    private _sprite: Sprite

    constructor(texture?: Texture<Resource>) {
        super()

        this._sprite = new Sprite(texture)
    }

    get body(): Graphics {
        return this._body
    }

    set body(options: {x: number, y: number, width: number, height: number}) {
        if ( !this._body ) this._body = new Graphics()
        else this._body.clear()

        this._body.beginFill(0x000000, 0.5)
        this._body.drawRect(options.x, options.y, options.width, options.height)
        this._body.endFill()

        this._body.alpha = 0
    }

    get sprite() {
        return this._sprite
    }
}