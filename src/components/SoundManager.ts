import { Howl, Howler } from 'howler'
import { callback } from '../helper'

const CONFIG: {name: string, volume?: number, loop?: boolean, onend?: callback}[] = [
    {name: 'ballon'},
    {name: 'barrier'},
    {name: 'button_press'},
    {name: 'coin'},
    {name: 'game_end', onend: () => {SoundManager.play('music')}},
    {name: 'jumper'},
    {name: 'list_show'},
    {name: 'music', volume: 0.5, loop: true},
    {name: 'snow', loop: true}
]

const sounds = {}

export class SoundManager {
    static init() {
        CONFIG.forEach(data => {
            let sound = new Howl({
                src: ['assets/sounds/' + data.name + '.mp3'],
                volume: data?.volume || 1,
                loop: data?.loop || false,
                onend: data.onend
            })

            sounds[data.name] = sound
        })
    }

    static play(name: string) {
        sounds[name]?.play()
    }

    static playing(name: string) {
        return sounds[name]?.playing()
    }

    static pause(name: string) {
        sounds[name]?.pause()
    }

    static stop(name: string) {
        sounds[name]?.stop()
    }

    static volume(power: number) {
        Howler.volume(power)
    }
}