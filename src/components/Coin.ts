import { Assets } from "pixi.js";
import { IActivated } from "../helper";
import { Obstacle } from "./Obstacle";

export class Coin extends Obstacle implements IActivated {
    activated: boolean = false

    constructor() {
        super(Assets.get('coin'))

        this.body = {
            x: -this.sprite.width / 2,
            y: -this.sprite.height * 1,
            width: this.sprite.width,
            height: this.sprite.height * 1
        }

        this.addChild(this.sprite, this.body)
    }
}