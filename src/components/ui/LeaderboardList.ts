import { Assets, Container, Sprite, Text } from "pixi.js";
import { FAKE_LEADERBOARD } from "../../helper";
import { gsap } from "gsap";
import { SoundManager } from "../SoundManager";

const leader_colors = [0xff6801, 0x2b5fa8, 0x2b5fa8]

export class LeaderboardList extends Container {
    list: Container[] = []

    constructor() {
        super()

        const a = new Sprite(Assets.get('place_1'))

        for(let n = 0; n < 3; n++) {
            const name_bar = new Sprite(Assets.get('place_' + (n + 1)))
            const score_bar = new Sprite(Assets.get('highleader_scores_plate'))
            const name = new Text(FAKE_LEADERBOARD[n].name, {
                fill: leader_colors[n],
                align: 'center',
                fontSize: 42,
                fontFamily: 'ZubiloBlack',
                padding: 10
            })
            const score = new Text(FAKE_LEADERBOARD[n].score, {
                fill: leader_colors[n],
                align: 'center',
                fontSize: 42,
                fontFamily: 'ZubiloBlack',
                padding: 10
            })

            name_bar.anchor.set(0.5)
            name_bar.position.set(-score_bar.width / 2 - 5, (name_bar.height + 2) * n)

            score_bar.anchor.set(0.5)
            score_bar.position.set(name_bar.x + name_bar.width / 2 + score_bar.width / 2 + 10, name_bar.y)

            name.anchor.set(0, 0.5)
            name.position.set(name_bar.x - name_bar.width / 2 + 90, name_bar.y)

            score.anchor.set(0.5)
            score.position.set(score_bar.x, score_bar.y)

            const cont = new Container()
            this.list.push(cont)
            cont.addChild(name_bar, name, score_bar, score)
            this.addChild(cont)
        }

        for(let n = 3; n < 10; n++) {
            const name_bar = new Sprite(Assets.get('midleader_name_plate'))
            const score_bar = new Sprite(Assets.get('midleader_scores_plate'))
            const place = new Text((n + 1), {
                fill: 0xffffff,
                align: 'center',
                fontSize: 36,
                fontFamily: 'ZubiloBlack',
                padding: 10
            })
            const name = new Text(FAKE_LEADERBOARD[n].name, {
                fill: 0x3d3c3d,
                align: 'center',
                fontSize: 36,
                fontFamily: 'ZubiloBlack',
                padding: 10
            })
            const score = new Text(FAKE_LEADERBOARD[n].score, {
                fill: 0x3d3c3d,
                align: 'center',
                fontSize: 36,
                fontFamily: 'ZubiloBlack',
                padding: 10
            })

            name_bar.anchor.set(0.5)
            name_bar.position.set(-score_bar.width / 2 + 25, (name_bar.height + 5) * (n - 3) + 230)

            score_bar.anchor.set(0.5)
            score_bar.position.set(name_bar.x + name_bar.width / 2 + score_bar.width / 2 + 10, name_bar.y)

            place.anchor.set(0.5)
            place.position.set(name_bar.x - name_bar.width / 2 - 30, name_bar.y)

            name.anchor.set(0, 0.5)
            name.position.set(name_bar.x - name_bar.width / 2 + 20, name_bar.y)

            score.anchor.set(0.5)
            score.position.set(score_bar.x, score_bar.y)

            const cont = new Container()
            this.list.push(cont)
            cont.addChild(place, name_bar, name, score_bar, score)
            this.addChild(cont)
        }

        this.hide()
    }

    refresh() {
        this.list.forEach((container, n) => {
            container.alpha = 0
            gsap.killTweensOf(container)
            gsap.to(container, {alpha: 1, duration: 0.1, onStart: () => {SoundManager.play('list_show')}}).delay(0.08 * n).play()
        })
    }

    hide() {
        this.list.forEach((container, n) => {
            container.alpha = 0
        })
    }
}