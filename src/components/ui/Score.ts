import { Assets, Container, Sprite, Text } from "pixi.js";

export class Score extends Container {
    count: number = 0
    number: Text

    constructor() {
        super()

        const coin = new Sprite(Assets.get('collect_coin_icon'))

        const text_bg = new Sprite(Assets.get('coin_score_plate'))
        text_bg.anchor.set(0, 0.5)
        text_bg.position.set(coin.x + coin.width / 2, coin.y + coin.height / 2)

        this.number = new Text(this.count, {
            fill: 0xffffff,
            align: 'center',
            fontSize: 52,
            fontFamily: 'ZubiloBlack'
        })
        this.number.anchor.set(0.5)
        this.number.position.set(text_bg.x + text_bg.width / 2 + 15, text_bg.y)

        this.addChild(text_bg, coin, this.number)
    }

    pickup() {
        this.count++
    }

    update() {
        this.number.text = this.count
    }

    reset() {
        this.count = 0

        this.update()
    }
}