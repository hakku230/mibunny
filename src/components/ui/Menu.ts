import { Assets, Container, Sprite, Text, Ticker } from "pixi.js";
import { LastScore } from "./LastScore";
import { LeaderboardMenu } from "./LeaderboardMenu";
import { NewScore } from "./NewScore";
import { gsap } from "gsap";
import { STARS_CONFIG } from "../../helper";

export class Menu extends Container {
    lastScore: LastScore
    leaderboard: LeaderboardMenu
    newScore: NewScore

    header_info_text: Text
    rays: Sprite
    stars: Sprite[]

    constructor() {
        super()

        this.scale.set(0.7)

        this.rays = new Sprite(Assets.get('rays'))
        this.rays.anchor.set(0.5)
        this.rays.visible = false
        this.stars = []
        STARS_CONFIG.forEach(config => {
            const star = new Sprite(Assets.get('star'))
            star.anchor.set(0.5)
            star.position.set(...config.pos)
            star.scale.set(config.scale)
            star.angle = Math.floor(Math.random() * 30) - 15
            star.visible = false
            gsap.to(star, {angle: star.angle + 15, duration: 2}).repeat(-1).yoyo(true).play()
            this.stars.push(star)
        })
        const ticker = new Ticker()
        ticker.add((delta) => {
            if (this.rays.visible) this.rays.angle += delta
        })
        ticker.start()

        const bg = new Sprite(Assets.get('info_plate_big'))
        bg.anchor.set(0.5)

        const header_info_plate = new Sprite(Assets.get('header_info_plate'))
        header_info_plate.anchor.set(0.5, 0)
        header_info_plate.position.set(0, -bg.height / 2 + 2)

        this.header_info_text = new Text('Твои рекорды:', {
            fill: 0x003d71,
            align: 'center',
            fontSize: 52,
            fontFamily: 'ZubiloBlack',
            padding: 10
        })
        this.header_info_text.anchor.set(0.5, 0)
        this.header_info_text.position.set(0, header_info_plate.y + 8)

        this.lastScore = new LastScore()
        this.lastScore.position.set(0, -this.lastScore.height / 2 + header_info_plate.height)

        this.leaderboard = new LeaderboardMenu()
        this.leaderboard.visible = false
        this.leaderboard.position.set(0, -this.leaderboard.height / 2 + header_info_plate.height - 10)

        this.newScore = new NewScore()
        this.newScore.visible = false
        this.newScore.position.set(0, -this.newScore.height / 2 + header_info_plate.height - 10)

        this.addChild(
            this.rays,
            ...this.stars,
            bg,
            header_info_plate,
            this.header_info_text,
            this.lastScore,
            this.leaderboard,
            this.newScore
        )

        document.addEventListener('showLeaderboard', this.showLeaderboard.bind(this))
        document.addEventListener('showLastScore', this.showLastScore.bind(this))
    }

    showNewScore() {
        this.header_info_text.text = 'Новый рекорд:'

        this.rays.visible = true
        this.stars.forEach(s => s.visible = true)
        this.newScore.visible = true

        this.leaderboard.visible = false
        this.lastScore.visible = false
    }

    showLeaderboard() {
        this.header_info_text.text = 'Таблица рекордов:'

        this.newScore.visible = false
        this.rays.visible = false
        this.stars.forEach(s => s.visible = false)

        this.lastScore.visible = false
        this.leaderboard.visible = true
        this.leaderboard.list.refresh()
    }

    showLastScore() {
        this.header_info_text.text = 'Твои рекорды:'

        this.leaderboard.visible = false
        this.lastScore.visible = true
    }
}