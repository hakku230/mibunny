import { Assets } from "pixi.js";
import { Button } from "./Button";
import { callback } from "../../helper";

export class TwoStateButton extends Button {
    state: number = 1
    base_texture_name

    constructor(base_texture_name: string, callback: callback = () => {}) {
        super(base_texture_name + '_1', callback)

        this.base_texture_name = base_texture_name
    }

    update(state: 'hover' | 'active' | 'press') {
        if ( state === 'press' ) {
            this.state = ++this.state % 2
            this.texture_name = this.base_texture_name + '_' + this.state
        }

        this.sprite.texture = Assets.get(this.texture_name + '_' + state)
    }
}