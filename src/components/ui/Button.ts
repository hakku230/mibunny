import { Assets, Container, Sprite } from "pixi.js";
import { callback } from "../../helper";
import { SoundManager } from "../SoundManager";

export class Button extends Container {
    texture_name: string
    callback: callback

    sprite: Sprite

    constructor(texture_name: string, callback: callback = () => {}) {
        super()

        this.texture_name = texture_name
        this.callback = callback

        this.sprite = new Sprite(Assets.get(texture_name + '_active'))

        this.addChild(this.sprite)

        this.sprite.eventMode = 'static'
        this.sprite.cursor = 'pointer'
        this.sprite.onpointerover = this.hover.bind(this)
        this.sprite.onpointerout = this.active.bind(this)
        this.sprite.onpointerdown = this.press.bind(this)
        this.sprite.onpointerup = this.hover.bind(this)
    }

    anchor(x: number, y?: number) {
        this.sprite.anchor.set(x, isNaN(y) ? x : y)
    }

    update(state: 'hover' | 'active' | 'press') {
        this.sprite.texture = Assets.get(this.texture_name + '_' + state)
    }

    hover() {
        this.update('hover')
    }

    active() {
        this.update('active')
    }

    press() {
        this.update('press')
        this.callback()

        SoundManager.play('button_press')
    }
}