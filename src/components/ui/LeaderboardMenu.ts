import { Container, Text } from "pixi.js";
import { Button } from "./Button";
import { LeaderboardList } from "./LeaderboardList";

const titles = ['Все время', 'Месяц', 'Неделя']

export class LeaderboardMenu extends Container {
    page: number = 0

    title: Text
    list: LeaderboardList

    constructor() {
        super()

        this.title = new Text(titles[this.page], {
            fill: 0xff6801,
            align: 'center',
            fontSize: 52,
            fontFamily: 'ZubiloBlack',
            padding: 10,
            dropShadow: true,
            dropShadowAlpha: 0.5,
            dropShadowAngle: 1.4,
            dropShadowDistance: 6
        })
        this.title.anchor.set(0.5)

        const prevBtn = new Button('arrow_btn', this.prev.bind(this))
        prevBtn.anchor(0.5)
        prevBtn.sprite.scale.set(-1, 1)
        prevBtn.position.set(-270, 0)

        const nextBtn = new Button('arrow_btn', this.next.bind(this))
        nextBtn.anchor(0.5)
        nextBtn.position.set(270, 0)

        this.list = new LeaderboardList()
        this.list.position.set(0, 70)

        const okBtn = new Button('ok_button', () => {
            document.dispatchEvent(new Event('showLastScore'))
        })
        okBtn.anchor(0.5)
        okBtn.position.set(0, 70 + this.list.height + 20)

        this.addChild(
            prevBtn,
            nextBtn,
            this.title,
            this.list,
            okBtn
        )
    }

    prev() {
        this.page = (this.page + 2) % 3
        this.title.text = titles[this.page]
        this.list.refresh()
    }

    next() {
        this.page = (this.page + 1) % 3
        this.title.text = titles[this.page]
        this.list.refresh()
    }
}