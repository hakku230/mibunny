import { Container } from "pixi.js";
import { Button } from "./Button";
import { TwoStateButton } from "./TwoStateButton";
import { toggleFullScreen } from "../../helper";
import { SoundManager } from "../SoundManager";

export class Controls extends Container {
    isPaused: boolean = false

    constructor() {
        super()

        const fullscreenBtn = new Button('btn_fullscreen', toggleFullScreen)
        const soundBtn = new TwoStateButton('btn_sound', () => {
            SoundManager.volume(soundBtn.state === 1 ? 1 : 0)
        })
        const pauseBtn = new Button('btn_pause', () => {
            document.dispatchEvent(new Event(!this.isPaused ? 'pause' : 'resume'))
        })

        soundBtn.x = fullscreenBtn.x + fullscreenBtn.width
        pauseBtn.x = soundBtn.x + soundBtn.width

        this.addChild(fullscreenBtn, soundBtn, pauseBtn)
    }
}