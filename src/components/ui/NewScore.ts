import { Assets, Container, Sprite, Text } from "pixi.js";
import { Button } from "./Button";

export class NewScore extends Container {
    score: Text
    coin_text: Text
    distance_text: Text

    constructor() {
        super()

        this.score = new Text(0, {
            fill: 0x00cc00,
            align: 'center',
            fontSize: 166,
            fontFamily: 'ZubiloBlack',
            padding: 10,
            dropShadow: true,
            dropShadowAlpha: 0.5,
            dropShadowAngle: 1.4,
            dropShadowDistance: 6
        })
        this.score.anchor.set(0.5)

        const coin_icon = new Sprite(Assets.get('coin'))
        coin_icon.anchor.set(0.5)
        coin_icon.position.set(-220, this.score.height)

        this.coin_text = new Text(0, {
            fill: 0xf4ad25,
            align: 'center',
            fontSize: 96,
            fontFamily: 'ZubiloBlack',
            padding: 10,
            dropShadow: true,
            dropShadowAlpha: 0.5,
            dropShadowAngle: 1.4,
            dropShadowDistance: 6
        })
        this.coin_text.anchor.set(0.5)
        this.coin_text.position.set(coin_icon.width / 2, coin_icon.y)

        const distance_icon = new Sprite(Assets.get('collect_distance_icon'))
        distance_icon.anchor.set(0.5)
        distance_icon.position.set(-220, coin_icon.y + coin_icon.height + 50)

        this.distance_text = new Text('0 м', {
            fill: 0x9ac6ff,
            align: 'center',
            fontSize: 96,
            fontFamily: 'ZubiloBlack',
            padding: 10,
            dropShadow: true,
            dropShadowAlpha: 0.5,
            dropShadowAngle: 1.4,
            dropShadowDistance: 6
        })
        this.distance_text.anchor.set(0.5)
        this.distance_text.position.set(distance_icon.width / 2, distance_icon.y)

        const okBtn = new Button('ok_button', () => {
            document.dispatchEvent(new Event('showLeaderboard'))
        })
        okBtn.anchor(0.5)
        okBtn.position.set(0, distance_icon.y + distance_icon.height + 100)

        this.addChild(
            this.score,
            coin_icon,
            this.coin_text,
            distance_icon,
            this.distance_text,
            okBtn
        )
    }
}