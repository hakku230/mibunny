import { Assets, Container, Sprite, Text } from "pixi.js";
import { Button } from "./Button";

export class LastScore extends Container {
    lastScoreText: Text

    constructor() {
        super()

        this.lastScoreText = new Text('Рекорд:\n0', {
            fill: 0x0deb21,
            align: 'center',
            fontSize: 52,
            fontFamily: 'ZubiloBlack',
            dropShadow: true,
            dropShadowAlpha: 0.5,
            dropShadowAngle: 1.4,
            dropShadowDistance: 6
        })
        this.lastScoreText.anchor.set(0.5)
        this.lastScoreText.position.set(0, 0)

        const xi_button = new Button('login_button')
        xi_button.anchor(0.5)
        xi_button.position.set(0, this.lastScoreText.y + this.lastScoreText.height + 70)

        const user_name_bar = new Sprite(Assets.get('user_name_bar'))
        user_name_bar.anchor.set(0.5)
        user_name_bar.position.set(0, xi_button.y + xi_button.height)

        const user_name_text = new Text('Guest_' + Math.floor(1000 + Math.random() * 9000), {
            fill: 0xffffff,
            align: 'center',
            fontSize: 52,
            fontFamily: 'ZubiloBlack',
            padding: 10
        })
        user_name_text.anchor.set(0, 0.5)
        user_name_text.position.set(-user_name_bar.width / 2 + 50, user_name_bar.y)

        const leadboard_button = new Button('leadboard_button', () => {
            document.dispatchEvent(new Event('showLeaderboard'))
        })
        leadboard_button.anchor(0.5, 1)
        leadboard_button.position.set(-160, user_name_bar.y + user_name_bar.height + 180)

        const play_button = new Button('play_button', () => {
            document.dispatchEvent(new Event('play'))
        })
        play_button.anchor(0.5, 1)
        play_button.position.set(160, user_name_bar.y + user_name_bar.height + 180)

        this.addChild(
            this.lastScoreText,
            xi_button,
            user_name_bar,
            user_name_text,
            leadboard_button,
            play_button
        )
    }

    update(score: number) {
        this.lastScoreText.text = 'Рекорд:\n' + score
    }
}