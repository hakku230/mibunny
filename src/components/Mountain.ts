import { Assets, Container, Sprite, Ticker, TilingSprite } from "pixi.js";
import { COIN_VERTICES, METER, SPEED } from "../helper";
import { Obstacle } from "./Obstacle";
import { Bunny } from "./Bunny";
import { Jumper } from "./Jumper";
import { Stopper } from "./Stopper";
import { Coin } from "./Coin";

export class Mountain extends Container {
    floor: TilingSprite
    jumpboard: Sprite
    column: Sprite
    pointer: Sprite
    check_point: Sprite

    trees: Sprite[] = []
    stoppers: Stopper[] = []
    jumper: Jumper
    coins: Obstacle[] = []

    environment: Sprite[] = []

    canPlay: boolean = false
    ticker: Ticker
    bunny: Bunny

    constructor() {
        super()

        const floor_asset = Assets.get('floor')
        this.floor = new TilingSprite(
            floor_asset,
            window.innerWidth,
            floor_asset.height,
        )
        this.floor.anchor.set(0, 0)
        this.floor.tilePosition.set(0, 0)
        this.floor.position.set(-50, -200)

        this.jumpboard = new Sprite(Assets.get('jumpboard'))
        this.jumpboard.anchor.set(0, 1)
        this.jumpboard.position.set(-50, this.floor.y + 10)

        this.addChild(this.jumpboard)

        for(let i = 0; i < 5; i++) {
            const tree = new Sprite(Assets.get(Math.random() > 0.5 ? 'tree_1' : 'tree_2'))
            tree.anchor.set(0.5, 1)
            tree.position.set(1200 + METER * Math.floor(Math.random() * 20), this.floor.y + 10)
            this.trees.push(tree)
            this.addChild(tree)
        }

        const jumper = new Jumper()
        jumper.sprite.anchor.set(0.5, 1)
        jumper.position.set(2400, this.floor.y + 10)
        this.jumper = jumper
        this.addChild(jumper)

        for(let i = 0; i < 2; i++) {
            const stopper = new Stopper()
            stopper.sprite.anchor.set(0.5, 1)
            stopper.position.set(2400 + 800 * (i + 1), this.floor.y + 10)
            this.stoppers.push(stopper)
            this.addChild(stopper)
        }

        this.column = new Sprite(Assets.get('element_2'))
        this.column.anchor.set(0.5, 1)
        this.column.position.set(600, this.floor.y + 10)
        this.addChild(this.column)

        this.pointer = new Sprite(Assets.get('element_1'))
        this.pointer.anchor.set(0.5, 1)
        this.pointer.position.set(600 + 10 * METER, this.floor.y + 10)
        this.addChild(this.pointer)

        this.check_point = new Sprite(Assets.get('check_point'))
        this.check_point.anchor.set(0.5, 1)
        this.check_point.position.set(600 + 20 * METER, this.floor.y + 10)
        this.addChild(this.check_point)

        this.environment.push(this.column, this.pointer, this.check_point)

        const current_height = COIN_VERTICES[Math.floor(Math.random() * COIN_VERTICES.length)]
        for(let i = 0; i < 5; i++) {
            const coin = new Coin()
            coin.sprite.anchor.set(0.5, 1)
            coin.position.set(2400 + 110 * i, this.floor.y - 40 * current_height[i])
            this.coins.push(coin)
            this.addChild(coin)
        }

        this.addChild(this.floor)

        this.bunny = new Bunny()
        this.addChild(this.bunny)

        this.angle = 3

        const ticker = new Ticker()
        ticker.add(this.proceed.bind(this))
        ticker.start()
        this.ticker = ticker
    }

    reset() {
        this.canPlay = false

        this.bunny.angle = 0

        this.jumpboard.position.set(-50, this.floor.y + 10)
        this.jumper.position.set(2400, this.floor.y + 10)
        this.jumper.activated = false

        this.stoppers.forEach((stopper, i) => {
            stopper.state = 'idle'
            stopper.position.set(2400 + 800 * (i + 1), this.floor.y + 10)
        })

        this.column.position.set(600, this.floor.y + 10)
        this.pointer.position.set(600 + 10 * METER, this.floor.y + 10)
        this.check_point.position.set(600 + 20 * METER, this.floor.y + 10)

        const current_height = COIN_VERTICES[Math.floor(Math.random() * COIN_VERTICES.length)]
        this.coins.forEach((coin, i) => {
            coin.visible = true
            coin.activated = false
            coin.position.set(2400 + 110 * i, this.floor.y - 40 * current_height[i])
        })

        this.trees.forEach(tree => tree.position.set(1200 + METER * Math.floor(Math.random() * 20), this.floor.y + 10))
    }

    proceed(delta) {
        if (!this.canPlay) return;

        this.floor.tilePosition.x -= delta * SPEED

        if ( this.jumpboard.x >= -this.jumpboard.width ) this.jumpboard.x -= delta * SPEED

        ;[...this.environment, ...this.trees, this.jumper, ...this.stoppers, ...this.coins].forEach(env => env.x -= delta * SPEED)

        this.trees.forEach(tree => {
            if ( tree.x < -tree.width ) {
                tree.x += window.innerWidth + METER * Math.floor(Math.random() * 5 + 5)
                tree.texture = Assets.get(Math.random() > 0.5 ? 'tree_1' : 'tree_2')
            }
        })

        if ( this.jumper.x < -this.jumper.width ) {
            this.jumper.x += 2000 + Math.floor(Math.random() * 800)
            this.jumper.activated = false
        }

        this.stoppers.forEach(stopper => {
            if ( stopper.x < -stopper.width ) {
                stopper.x += 2000 + Math.floor(Math.random() * 800)
            }
        })

        const last_coin = this.coins[this.coins.length - 1]
        if ( last_coin.x < -last_coin.width ) {
            const current_height = COIN_VERTICES[Math.floor(Math.random() * COIN_VERTICES.length)]
            const deltaX = 2000 + Math.floor(Math.random() * 800)

            this.coins.forEach((coin, n) => {
                coin.visible = true
                coin.activated = false
                coin.position.set(coin.x + deltaX, this.floor.y - 40 * current_height[n])
            })
        }

        if ( this.column.x < -this.column.width ) this.column.x += METER * 30
        if ( this.pointer.x < -this.pointer.width ) this.pointer.x += METER * 30
        if ( this.check_point.x < -this.check_point.width ) this.check_point.x += METER * 30
    }

    resize() {
        this.floor.width = window.innerWidth + 100

        if ( window.innerWidth < 1280 ) {
           this.scale.set(window.innerWidth / 1280)
           this.floor.width = window.innerWidth / (window.innerWidth / 1280) + 100
        }

        if ( window.innerHeight < 800 ) {
            this.scale.set(0.5)
            this.floor.width = window.innerWidth / (0.5) + 100
        }

        if ( window.innerHeight < 600 ) {
            this.scale.set(0.4)
            this.floor.width = window.innerWidth / (0.4) + 100
        }
    }
}