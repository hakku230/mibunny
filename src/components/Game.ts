import { Application, Assets, Container, Graphics, Sprite, Ticker, TilingSprite } from "pixi.js"
import { Score } from './ui/Score'
import { Controls } from './ui/Controls'
import { Menu } from './ui/Menu'
import { Mountain } from './Mountain'
import { gsap } from "gsap";
import { SPEED, collision } from './../helper'
import { SoundManager } from "./SoundManager"

const JUMP_LENGTH = 90

export class Game {
    static _instance: Game

    application: Application

    score: Score
    controls: Controls
    menu: Menu

    bg: TilingSprite

    mountain: Mountain

    jump: number = 0
    dash: boolean = false
    ticker: Ticker
    distance: number = 0

    isGameover: boolean = false
    overlay: Graphics
    clickArea: Graphics

    constructor(application: Application) {
        this.application = application

        this.score = new Score()
        this.controls = new Controls()
        this.menu = new Menu()

        const bg_asset = Assets.get('back_rocks')
        this.bg = new TilingSprite(
            bg_asset,
            application.screen.width,
            bg_asset.height,
        )
        this.bg.anchor.set(0, 1)
        this.bg.tilePosition.set(0, 0)

        const sun = new Sprite(Assets.get('bg_sun'))
        sun.position.set(100, 50)

        this.mountain = new Mountain()

        this.overlay = new Graphics()
        this.overlay.beginFill(0x000000, 0.5)
        this.overlay.drawRect(0, 0, window.innerWidth, window.innerHeight)
        this.overlay.endFill()

        this.clickArea = new Graphics()
        this.clickArea.beginFill(0x000000, 0.5)
        this.clickArea.drawRect(0, 0, window.innerWidth, window.innerHeight)
        this.clickArea.endFill()
        this.clickArea.alpha = 0
        this.clickArea.eventMode = 'static'

        this.application.stage.addChild(this.bg, sun, this.mountain, this.overlay, this.clickArea, this.menu, this.score, this.controls)

        window.onresize = this.resize.bind(this)

        this.resize()

        this.ticker = new Ticker()
        this.ticker.add(this.gravity.bind(this))
        this.ticker.add(this.collisions.bind(this))
        this.ticker.start()

        document.addEventListener('play', this.play.bind(this))
        document.addEventListener('pause', this.pause.bind(this))
        document.addEventListener('resume', this.resume.bind(this))

        window.onkeydown = e => {
            if ( e.code == 'Space' ) this.doJump()
        }
        this.clickArea.onpointerdown = this.doJump.bind(this)

        SoundManager.play('music')
    }

    pause() {
        if ( this.mountain.canPlay && !this.isGameover ) {
            this.controls.isPaused = true
            this.ticker.stop()
            this.mountain.ticker.stop()
            this.overlay.visible = true

            SoundManager.pause('snow')
        }
    }

    resume() {
        if ( this.mountain.canPlay && !this.isGameover ) {
            this.controls.isPaused = false
            this.ticker.start()
            this.mountain.ticker.start()
            this.overlay.visible = false
        }
    }

    collisions() {
        if (!this.mountain.canPlay) return;

        if ( !this.mountain.jumper.activated && collision(this.mountain.bunny.body, this.mountain.jumper.body) ) {
            this.mountain.jumper.activated = true
            this.jump = JUMP_LENGTH
            this.dash = false

            SoundManager.play('jumper')
        }

        this.mountain.stoppers.forEach(stopper => {
            if ( collision(this.mountain.bunny.body, stopper.body) ) {
                stopper.state = 'crush'
                this.gameover()

                SoundManager.play('barrier')
            }
        })

        this.mountain.coins.forEach(coin => {
            if ( !coin.activated && collision(this.mountain.bunny.body, coin.body) ) {
                coin.visible = false
                coin.activated = true
                const temporary_coins = new Sprite(Assets.get('coin'))
                temporary_coins.scale.set(this.mountain.scale.x, this.mountain.scale.y)
                temporary_coins.anchor.set(0)
                temporary_coins.position.set(coin.getBounds().x, coin.getBounds().y)
                this.application.stage.addChild(temporary_coins)

                this.score.pickup()
                gsap.to(temporary_coins.scale, {x: this.score.scale.x, y: this.score.scale.y, duration: 0.99})
                gsap.to(temporary_coins, {x: this.score.x, y: this.score.y, duration: 1, onComplete: () => {
                    this.score.update()
                    this.application.stage.removeChild(temporary_coins)
                    temporary_coins.destroy()
                }})

                SoundManager.play('coin')
            }
        })
    }

    gameover() {
        this.isGameover = true

        this.ticker.stop()
        this.mountain.ticker.stop()

        const calculated_distance = Math.floor(this.distance / 250)
        const score_points = 5 * this.score.count + calculated_distance

        this.menu.newScore.score.text = score_points
        this.menu.newScore.coin_text.text = this.score.count
        this.menu.newScore.distance_text.text = calculated_distance + ' м'
        this.menu.lastScore.update(score_points)

        this.overlay.visible = true
        this.menu.visible = true
        this.menu.showNewScore()

        SoundManager.stop('snow')
        SoundManager.stop('music')
        SoundManager.play('game_end')
    }

    play() {
        if ( this.isGameover ) this.restart()

        this.menu.visible = false
        this.overlay.visible = false

        this.mountain.bunny.downhill(() => {
            this.mountain.canPlay = true
            this.jump = JUMP_LENGTH
        })

        SoundManager.play('snow')
    }

    restart() {
        this.distance = 0
        this.dash = false
        this.jump = 0

        this.score.reset()
        this.mountain.reset()

        this.resize()

        this.isGameover = false

        this.ticker.start()
        this.mountain.ticker.start()
    }

    resize() {
        this.score.position.set(10, 10)
        this.controls.position.set(window.innerWidth - this.controls.width, 0)
        this.menu.position.set(window.innerWidth / 2, window.innerHeight / 2)

        this.bg.width = window.innerWidth
        this.bg.position.set(0, window.innerHeight)

        this.mountain.position.set(0, window.innerHeight)
        this.mountain.resize()

        this.overlay.width = window.innerWidth 
        this.overlay.height = window.innerHeight
        this.clickArea.width = window.innerWidth
        this.clickArea.height = window.innerHeight

        this.menu.scale.set(Math.min(0.7, window.innerHeight / 950))
        this.score.scale.set(Math.min(1, window.innerHeight / 950))
        this.controls.scale.set(Math.min(1, window.innerHeight / 950))

        if (!this.mountain.canPlay) this.mountain.bunny.position.set(this.mountain.bunny.width / 2, this.mountain.floor.y - this.mountain.jumpboard.height + 50)
    }

    gravity(delta) {
        if (!this.mountain.canPlay) return;

        this.distance += delta * SPEED

        if (this.jump > 0) this.jump -= delta
        else this.jump = 0

        const lower_point = this.mountain.floor.y + 10

        if ( this.mountain.bunny.y < lower_point || this.jump > 0 ) {
            this.mountain.bunny.y += (this.jump > 0) ? (-delta * this.jump / 8) : (delta * (this.dash ? 12 : 3))

            if ( SoundManager.playing('snow') ) SoundManager.pause('snow')
        } else {
            this.mountain.bunny.y = lower_point
            this.dash = false

            if ( !SoundManager.playing('snow') ) SoundManager.play('snow')
        }

        this.mountain.bunny.angle = (this.mountain.bunny.y < lower_point) ? ( Math.abs(Math.abs(this.mountain.bunny.y) - Math.abs(lower_point)) / Math.abs(this.mountain.bunny.y) * (this.dash ? 30 : -30)) : 0
    }

    isBunnyLanded() {
        const lower_point = this.mountain.floor.y + 10

        if ( this.mountain.bunny.y < lower_point || this.jump > 0 ) return false

        return true
    }

    doJump() {
        if (!this.mountain.canPlay || this.controls.isPaused) return;

        const lower_point = this.mountain.floor.y + 10
        
        if ( this.mountain.bunny.y < lower_point ) {
            this.dash = true
            this.jump = 0
        } else {
            this.dash = false
            this.jump = JUMP_LENGTH
        }
    }
}