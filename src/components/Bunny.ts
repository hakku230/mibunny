import { Assets, Container, Graphics, Sprite } from "pixi.js";
import { gsap } from "gsap";
import { callback } from "../helper";
import { Obstacle } from "./Obstacle";

export class Bunny extends Obstacle {
    constructor() {
        super(Assets.get('bunny'))

        this.sprite.anchor.set(0.5, 0.88)
        this.sprite.scale.set(0.5)

        this.body = {
            x: -this.sprite.width / 4,
            y: -this.sprite.height * 0.8,
            width: this.sprite.width / 2,
            height: this.sprite.height * 0.75
        }

        this.addChild(this.sprite, this.body)
    }

    downhill(callback: callback) {
        let currentY = this.y

        let seq = gsap.timeline()

        seq.to(this, {x: 200, y: currentY += 50, angle: 60, duration: 0.5, ease: 'none'})
        seq.to(this, {x: 300, y: currentY += 200, angle: 50, duration: 0.25, ease: 'none'})
        seq.to(this, {x: 500, y: currentY += 200, angle: 40, duration: 0.25, ease: 'none'})
        seq.to(this, {x: 650, y: currentY += 20, angle: 0, duration: 0.1, ease: 'none'})
        seq.to(this, {x: 700, y: currentY += 20, angle: -10, duration: 0.1, ease: 'none', onComplete: callback})
    }
}