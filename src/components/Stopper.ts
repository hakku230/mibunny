import { Assets } from "pixi.js";
import { IActivated } from "../helper";
import { Obstacle } from "./Obstacle";

export class Stopper extends Obstacle {
    _state: 'idle' | 'crush' = 'idle'

    constructor() {
        super(Assets.get('stopper_idle'))

        this.body = {
            x: -this.sprite.width * 0.4,
            y: -this.sprite.height * 0.9,
            width: this.sprite.width * 0.8,
            height: this.sprite.height * 0.9
        }

        this.addChild(this.sprite, this.body)
    }

    set state(state: 'idle' | 'crush') {
        if ( this._state === state ) return;
        this._state = state

        this.sprite.texture = Assets.get('stopper_' + this._state)
    }
}