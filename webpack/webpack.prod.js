const HtmlWebpackPlugin = require('html-webpack-plugin'); // Require  html-webpack-plugin plugin
const CopyPlugin = require("copy-webpack-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const WebpackObfuscator = require('webpack-obfuscator');
const path = require("path");

module.exports = {
  mode: 'production',
  entry: "./src/index.ts", // webpack entry point. Module to start building dependency graph
  output: {
    path: path.resolve(__dirname, '../dist'), // Folder to store generated bundle
    filename: '[name].bundle.js',
    chunkFilename: '[name].chunk.js',
    publicPath: '' // public URL of the output directory when referenced in a browser
  },
  resolve: {
      extensions: ['.ts', '.tsx', '.js']
  },
  optimization: {
      splitChunks: {
          cacheGroups: {
              commons: {
                  test: /[\\/]node_modules[\\/]/,
                  name: 'vendors',
                  chunks: 'all',
                  filename: '[name].bundle.js'
              }
          }
      },
      minimize: true,
      minimizer: [
        new TerserPlugin({
          parallel: true,
          terserOptions: {
            compress: true,
          },
        }),
      ],
  },
  module: {  // where we defined file patterns and their loaders
      rules: [
        {
          test: /\.m?js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env'],
              plugins: ['@babel/plugin-proposal-class-properties']
            }
          }
        },
        {
          test: /\.(png|jpe?g|gif)$/i,
          use: [
            {
              loader: 'file-loader',
            },
          ],
        },
        {
          test: /\.csv$/,
          loader: 'csv-loader',
          options: {
            dynamicTyping: true,
            header: true,
            skipEmptyLines: true
          }
        },
        {
          test: /\.tsx?$/,
          use: [
            {
              loader: 'ts-loader',
              options: {
                transpileOnly: true
              }
            }
          ],
          exclude: /node_modules/,
        },
      ]
  },
  plugins: [  // Array of plugins to apply to build chunk
      new HtmlWebpackPlugin({
          template: "./src/index.html",
          inject: 'body'
      }),
      new CopyPlugin({
        patterns: [
          { from: "src/assets", to: "assets" },
          { from: "src/css", to: "css" },
        ],
      }),
      new WebpackObfuscator ({
        rotateStringArray: true,
        identifierNamesGenerator: 'mangled'
      }, [])
  ]
};